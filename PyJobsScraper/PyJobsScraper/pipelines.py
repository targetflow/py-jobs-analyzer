# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
#
import pymongo

class PyjobsscraperPipeline(object):
    def process_item(self, item, spider):
        return item


class MongoDBPipelineDouBot(object):
    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DB')
        )

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    def close_spider(self, spider):
        self.client.close()

    def process_item(self, item, spider):
        # вирішує питання дублікатів
        self.db[spider.settings.get('COLLECTION_NAME')].update({'id': item['id']}, {"$setOnInsert": item}, upsert=True)

        # вирішує питання категорій
        self.db['vacancies'].update({"$and": [{'id': item['id'], 'categories': {"$nin": item['categories']}}]},
                                                               {"$push": {'categories': " "+spider.category}})
        return item

class MongoDBPipelineDouCompaniesBot(object):
    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DB')
        )

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    def close_spider(self, spider):
        self.client.close()

    def process_item(self, item, spider):
        # вирішує питання дублікатів
        self.db['companies'].update({'name': item['name']}, {"$setOnInsert": item},
                                                               upsert=True)
        return item

