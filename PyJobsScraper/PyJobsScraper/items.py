# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader import ItemLoader
from .constants import  RU_MONTH_VALUES
from datetime import datetime

class DouItem(scrapy.Item):
    categories = scrapy.Field()
    title = scrapy.Field()
    company = scrapy.Field()
    company_url = scrapy.Field()
    location = scrapy.Field()
    pub_date = scrapy.Field()
    url = scrapy.Field()
    salary = scrapy.Field()
    id = scrapy.Field()
    required_skills = scrapy.Field()
    plus_skills = scrapy.Field()
    offer = scrapy.Field()
    about_proj = scrapy.Field()
    time_of_update = scrapy.Field()


class CompanyItem(scrapy.Item):
    name = scrapy.Field()
    city = scrapy.Field()
    number_of_specialists = scrapy.Field()
    href = scrapy.Field()
    about_comp = scrapy.Field()
    time_of_update = scrapy.Field()
    response_number = scrapy.Field()
    vacancies_open = scrapy.Field()
    contacts = scrapy.Field()
    rating = scrapy.Field()


# processors
def int_value_from_ru_month(date_str):
    date_str = ''.join(date_str)
    for k, v in RU_MONTH_VALUES.items():
        date_str = date_str.replace(k, str(v))
    return date_str


def pub_date_convert_processor(self, value):
    date_list = int_value_from_ru_month(value).split(' ')
    date_format = datetime(int(date_list[2]), int(date_list[1]), int(date_list[0]))
    date = date_format.strftime("%Y-%m-%d")
    return date


# loaders
class CompanyLoader(ItemLoader):
    default_item_class = CompanyItem


class DouLoader(ItemLoader):
    default_item_class = DouItem
    pub_date_in = pub_date_convert_processor
