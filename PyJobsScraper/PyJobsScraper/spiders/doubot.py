# -*- coding: utf-8 -*-
from .. import items
import scrapy
from datetime import datetime


class DouSpider(scrapy.Spider):
    name = "dou"
    allowed_domains = ["jobs.dou.ua"]
    custom_settings = {
        'COLLECTION_NAME': 'vacancies',
        'ITEM_PIPELINES': {
            'PyJobsScraper.pipelines.MongoDBPipelineDouBot': 300
        }
    }

    # scrapy crawl dou -a category=python - приклад
    # scrapy crawl dou -a category=Data+Science - приклад, якщо запит складається з більш ніж одного слова
    def __init__(self, category='python', *args, **kwargs):
        super().__init__(**kwargs)
        self.category = category.lower()
        self.start_urls = ['https://jobs.dou.ua/vacancies/?search=%s' % category]

    def parse(self, response):
        for href, id in zip(response.xpath('//a[@class="vt"]/@href').extract(),
                            response.xpath('//div[@class = "vacancy"]/@_id').extract()):
            yield scrapy.Request(href, callback=self.parse_dir_contents, meta={'id': id})

    def parse_dir_contents(self, response):
        l = items.DouLoader(item=items.DouItem(), response=response)
        l.add_value('categories', self.category)
        l.add_value('id', response.meta['id'])
        l.add_xpath('title','//h1[@class = "g-h2"]/text()' )
        l.add_xpath('company','//div[@class = "l-n"]/a[1]/text()')
        l.add_xpath('company_url', '//div[@class = "l-n"]/a[1]/@href')
        l.add_xpath('location', '//span[@class = "place"]/text()')
        l.add_value('url', response.request.url)
        l.add_xpath('pub_date', '//div[@class = "date"]/text()')
        l.add_xpath('salary', '//span[@class = "salary"]/text()')
        l.add_value('time_of_update', str(datetime.now().strftime("%Y-%m-%d %H:%M:%S")))

        l.add_xpath('required_skills', '//h3[contains(text(), "Необходимые навыки")]/following-sibling::div[1]/p/text()')
        if l.get_output_value('required_skills') :
            l.add_xpath('plus_skills', '//h3[contains(text(), "Будет плюсом")]/following-sibling::div[1]/p/text()')
            l.add_xpath('offer', '//h3[contains(text(), "Предлагаем")]/following-sibling::div[1]/p/text()')
            l.add_xpath('about_proj', '//h3[contains(text(), "О проекте")]/following-sibling::div[1]/p/text()')
        else:
            l.add_value('plus_skills', [])
            l.add_value('offer', [])
            l.add_xpath('about_proj', '//div[@class ="text b-typo vacancy-section"]/p/text()')


        yield l.load_item()