# -*- coding: utf-8 -*-
import datetime
from pymongo import MongoClient
from .. import items, settings
import scrapy
from datetime import datetime


class DouSpider(scrapy.Spider):
    name = "dou-companies"
    allowed_domains = ["jobs.dou.ua"]
    custom_settings = {
        'COLLECTION_NAME': 'companies',
        'ITEM_PIPELINES': {
             'PyJobsScraper.pipelines.MongoDBPipelineDouCompaniesBot': 300}
    }

    def start_requests(self):
        client = MongoClient(settings.MONGO_URI)
        db = client[settings.MONGO_DB]
        list_of_company_url = db.vacancies.distinct('company_url')
        if list_of_company_url:
            for url in list_of_company_url:
                yield scrapy.Request(url, callback=self.parse)
        else:
            print("\n\n\n You need to run dou bot first. Reason: we are interested in companies, which vacancies could "
                  "be parsed with dou bot.\n\n\n")

    def parse(self, response):
        company_load = items.CompanyLoader(item=items.CompanyItem(), response=response)
        company_load.add_xpath('name', '//h1[@class = "g-h2"]/text()')
        company_load.add_xpath('city', '//div[@class = "offices"]/text()')
        company_load.add_xpath('number_of_specialists', '//h1[@class = "g-h2"]/following-sibling::text()[1]')
        company_load.add_xpath('href', '//div[@class="site"]/a/@href')
        text = [
            ' '.join(
                line.strip()
                for line in p.xpath('.//text()').extract()
                if line.strip()
            )
            for p in response.xpath('//div[@class="b-typo"]//p')
        ]
        company_load.add_value('about_comp',text)
        rating = {'company_rating': response.xpath('//h3[contains(text(), "Оценка компании:")]/text()').extract(),
                  'voted_people': response.xpath('//div[@class="ps"]/a[1]/text()').extract(),
                  'compensation': response.xpath('//div[contains(text(), "Компенсация")]'
                                                 '/following-sibling::div[1]/text()').extract(),
                  'working_conditions': response.xpath('//div[contains(text(), "Условия труда")]'
                                                       '/following-sibling::div[1]/text()').extract(),
                  'career': response.xpath('//div[contains(text(), "Карьера")]'
                                           '/following-sibling::div[1]/text()').extract(),
                  'project': response.xpath('//div[contains(text(), "Проект")]'
                                            '/following-sibling::div[1]/text()').extract(),
                  'loyalty': response.xpath('//div[contains(text(), "Лояльность")]'
                                            '/following-sibling::div[1]/text()').extract()}

        company_load.add_value('rating', rating)

        company_load.add_value('time_of_update', str(datetime.now()))
        next_page = response.xpath('///ul[@class="company-nav"]/li[contains(.,"Отзывы")]/a[1]/@href').get()
        yield scrapy.Request(next_page, callback=self.parse_reviews, meta={'item': company_load.load_item()})

    def parse_reviews(self, response):
        loader_company_review = items.CompanyLoader(item=response.meta['item'], response=response)
        loader_company_review.add_xpath('response_number', '//h3[@id="lblCommentsCount"]/text()')
        next_page = response.xpath('///ul[@class="company-nav"]/li[contains(.,"Вакансии")]/a[1]/@href').get()
        yield scrapy.Request(next_page, callback=self.parse_open_vacancies,
                             meta={'item': loader_company_review.load_item()})

    def parse_open_vacancies(self, response):
        loader_company_open_v = items.CompanyLoader(item=response.meta['item'], response=response)
        loader_company_open_v.add_xpath('vacancies_open',
                                        '//div[@class = "b-inner-page-header"]/h1[1]/text()')
        next_page = response.xpath('///ul[@class="company-nav"]/li[contains(.,"Офисы")]/a[1]/@href').get()
        yield scrapy.Request(next_page, callback=self.parse_contacts,
                             meta={'item': loader_company_open_v.load_item()})

    def parse_contacts(self, response):
        loader_contacts = items.CompanyLoader(item=response.meta['item'], response=response)
        contacts = []
        contact = {}
        for li in response.xpath('//ul[@class="persons"]/li'):
            contact["name"] = li.xpath('a[@class="name"]/text()').extract()
            contact["post"] = li.xpath('text()').extract()
            contact["link"] = li.xpath('a[@class="name"]/@href').extract()
            contacts.append(contact)
        loader_contacts.add_value('contacts', contacts)
        yield loader_contacts.load_item()






